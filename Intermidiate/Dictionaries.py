dictionary = {"name": "Max", "age": 28, "city": "New York"}

for key in dictionary:
    print(key)

print(dictionary)

del dictionary["name"]
print(dictionary.pop("age"))
