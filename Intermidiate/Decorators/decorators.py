def start_and_end(func):

    def wrapper(*args, **kwargs):
        print("Start")
        result_function = func(*args, **kwargs)
        print("end")
        return result_function
    return wrapper


@start_and_end
def add5(x):
    return x + 5


result = add5(10)
print(result)

