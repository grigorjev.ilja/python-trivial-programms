myList = ["banana", "cherry", "apple"]

print(myList)

for i in myList:
    print(i)


if "banana" in myList:
    print("Banana is here!")
else:
    print("Sorry, no banana")

myList.append("lemon")
print(myList)

print(myList.pop(2))
print(myList)

myList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(myList)

list_original = myList.copy()
list_original.append("pome")
print(list_original)

one_liner = [x * x for x in myList]
print(one_liner)
