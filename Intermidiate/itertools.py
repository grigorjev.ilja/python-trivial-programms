from itertools import product, groupby
a = [1, 2]
b = [3, 4]
prod = product(a, b)
print(list(prod))


persons = [{'name': 'Tim', 'age': 25}, {'name': 'Ilja', 'age': 25},
           {'name': 'Igor', 'age': 25}, {'name': 'Tim', 'age': 27}]


group_by = groupby(persons, key=lambda x: x['age'])
for key, value in group_by:
    print(key, list(value))
