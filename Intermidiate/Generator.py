def myGenerator():
    yield 1
    yield 2
    yield 3

g = myGenerator()
print(g)

value = next(g)
print(value)


def first_generator(n):
    num = 0
    while num < n:
        yield num
        num += 1
print(sum(first_generator(10)))