add_10 = lambda x: x + 10
print(add_10(5))

multiply = lambda x,y: x * y
print(multiply(132, 134))

points2D = [(1, 2), (15, 1), (5, -1), (10, 4)]
points2D_sorted = sorted(points2D, key=lambda x: x[1])

print(points2D)
print(points2D_sorted)

a = [1, 2, 3, 4, 5]
b = map(lambda x: x/2, a)
print(list(b))
