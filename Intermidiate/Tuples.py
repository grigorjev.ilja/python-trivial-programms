mytuple = ("Max", )
print(type(mytuple))

mytuple2 = ('a', 'x', 'dwda', 'w')
print(type(mytuple2))

item = mytuple2[0]
print(item)

if "Max" in mytuple:
    print("Yes")
else:
    print("No")

print(len(mytuple))

print(mytuple.count('a'))
print(mytuple2.index('x'))

