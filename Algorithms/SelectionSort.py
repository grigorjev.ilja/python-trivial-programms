
def selection(data):
    for i, e in enumerate(data):
        mn = min(range(i, len(data)), key=data.__getitem__)
        data[i], data[mn] = data[mn], e
    return data


data = [1, 2, 12, 3, 4, 5, 6, 101000, 1010011001, 11100101]
print(selection([1, 2, 12, 3, 4, 5, 6, 101000, 1010011001, 11100101]))
