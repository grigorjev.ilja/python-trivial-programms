
def binary_search(binary_search_list, item):
    low = 0
    high = len(binary_search_list) - 1
    while low <= high:
        mid = int((high + low) / 2)
        guess = binary_search_list[mid]
        if guess == item:
            return mid
        if guess > item:
            high = mid - 1
        else:
            low = mid + 1
    return None


my_list = [1, 2, 4, 5, 6, 7, 8, 9, 10]
print(binary_search(my_list, 5))
